import React, { PureComponent } from 'react';

class Card extends PureComponent {
  render() {
    let { isSelected, onClick, movie } = this.props;
    return (
      <div className={`movieCard ${isSelected ? 'selectedCard' : ''}`} onClick={onClick}>
        <div className="eventDimension">
          <label>{movie.EventDimension}</label>
        </div>
        {isSelected && <div className="selectedIndicator" />}
        <div className="poster">
          <img src={`https://in.bmscdn.com/events/moviecard/${movie.EventCode}.jpg`} alt={movie.EventCode} />
        </div>
        <div className="detail">
          <div className="movieTitle">
            <label>{movie.EventTitle}</label>
          </div>
          <div className="actorName">
            <div>
              <label className="labelTag">Likes:</label>
              <label className="labelTag">{movie.csCount}</label>
            </div>
            <div>
              <label className="labelTag">Rating:</label>
              <label className="labelTag">{movie.avgRating}</label>
            </div>
          </div>

          <div className="actorName">
            <div>
              {movie.EventGenre &&
                movie.EventGenre.split('|').map(item => (
                  <label key={item} className="genreLabel">
                    {item}
                  </label>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Card.defaultProps = {
  isSelected: false,
  onClick: () => {},
  movie: {}
};

export default Card;
