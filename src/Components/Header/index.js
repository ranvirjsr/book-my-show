import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

class Header extends PureComponent {
  render() {
    return (
      <div className="headerWrapper">
        <Link to="/" className="rightHeaderItem">
          Home
        </Link>
        <div className="rightHeader">
          <Link to="/movies" className="rightHeaderItem">
            Movies
          </Link>
        </div>
      </div>
    );
  }
}

export default Header;
