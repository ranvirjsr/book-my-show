import React, { PureComponent } from 'react';
import ReactPlayer from 'react-player';

class Detail extends PureComponent {
  render() {
    let { playerWidth, playerHeight, movie, close } = this.props;
    return (
      <div className="movieDetailContainer">
        <div onClick={close} className="closeButton">
          &#x2715;
        </div>
        <div className="movieVideoWrapper">
          <ReactPlayer url={movie.TrailerURL} playing className="videoPlayer" height={playerHeight} width={playerWidth} />
        </div>
        <div className="movieDetailWrapper">
          <div className="actorName">
            <label className="movieName">{movie.EventTitle}</label>
          </div>
          <div className="actorName">
            <label className="labelTag">{movie.EventLanguage}</label>
          </div>
          <div className="actorName">
            <div>
              <label className="labelTag">Likes:</label>
              <label className="labelTag">{movie.csCount}</label>
            </div>
          </div>
          <div className="actorName">
            <div>
              <label className="labelTag">Rating:</label>
              <label className="labelTag">{movie.avgRating}</label>
            </div>
          </div>
          <div className="actorName">
            <div>
              {movie.EventGenre &&
                movie.EventGenre.split('|').map(item => (
                  <label key={item} className="genreLabel">
                    {item}
                  </label>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Detail.defaultProps = {
  playerWidth: null,
  playerHeight: null,
  movie: {},
  close: () => {}
};
export default Detail;
