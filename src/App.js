import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import Container from './Container';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Container />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
