const headers = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};
const CARD_WIDTH = 232;
const CARD_MARGIN = 20;

const makeAPICall = async (endPoint, data = {}, type = 'GET') => {
  const apiObj = {
    method: type,
    headers: headers
  };
  return fetch(endPoint, apiObj)
    .then(res => res.json())
    .catch(e => {
      console.log('error', e);
      return null;
    });
};

export { makeAPICall, CARD_WIDTH, CARD_MARGIN };
