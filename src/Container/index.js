import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import MovieList from './MovieList';
import PlayMovie from './MovieList';
import Header from '../Components/Header';

class Container extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="bodyWrapper">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/movies" component={MovieList} />
            <Route path="/play" component={PlayMovie} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default Container;
