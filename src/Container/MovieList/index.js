import React, { Component } from 'react';
import { CARD_WIDTH, CARD_MARGIN } from '../../common';
import data from '../../mock/index.json';
import _ from 'lodash';
import Card from '../../Components/Card';
import Detail from '../../Components/Detail';
class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPlayer: false,
      selectedMovie: {},
      width: 0,
      height: 0,
      selectedRow: null,
      selectedCol: null
    };
    this.goToPlay = this.goToPlay.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  componentWillMount() {
    //making api call
    // makeAPICall('https://in.bookmyshow.com/serv/getData?cmd=GETTRAILERS&mtype=cs')
    //   .then(data => {
    //     console.log('data', data);
    //   })
    //   .then(e => {
    //     console.log('error', e);
    //   });
  }
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  goToPlay(e, selectedRow, selectedCol, selectedMovie) {
    e.preventDefault();
    this.setState({ selectedMovie, showPlayer: true, selectedRow, selectedCol });
  }
  onClose(e) {
    e.preventDefault();
    this.setState({ selectedMovie: {}, showPlayer: false, selectedRow: null, selectedCol: null });
  }

  renderMoviePlayer(index) {
    let { selectedMovie, selectedRow, width, height } = this.state;

    if (index === selectedRow) {
      return <Detail playerWidth={(width * 60) / 100} playerHeight={(height * 60) / 100} movie={selectedMovie} close={this.onClose} />;
    } else {
      return null;
    }
  }

  render() {
    let allMovies = data[1];
    let numOfColumn = Math.trunc(this.state.width / (CARD_WIDTH + CARD_MARGIN));
    allMovies = _.chunk(_.map(allMovies), numOfColumn);
    let { selectedCol, selectedRow } = this.state;
    return (
      <div className="mainContainer">
        <div className="movieWrapper">
          {_.map(allMovies, (movies, row) => {
            return (
              <div className="containerWrapper" key={row}>
                {this.renderMoviePlayer(row)}
                <div className="movieRow">
                  {movies.map((movie, index) => {
                    return <Card isSelected={selectedCol === index && selectedRow === row} movie={movie} key={index + row} onClick={e => this.goToPlay(e, row, index, movie)} />;
                  })}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default MovieList;
