import React, { Component } from 'react';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      duplicate: [],
      unique: []
    };
    this.setText = this.setText.bind(this);
    this.addData = this.addData.bind(this);
    this.validate = this.validate.bind(this);
    this.save = this.save.bind(this);
  }

  setText(e) {
    e.preventDefault();
    this.setState({ input: e.target.value });
  }

  addData(e) {
    e.preventDefault();
    let input = this.state.input;
    this.setState({ duplicate: [] }, () => {
      if (input.indexOf(',') > -1) {
        let inputs = input.split(',');
        inputs.map(item => {
          this.validate(item);
          return null;
        });
      } else {
        this.validate(input);
      }
    });
  }

  validate(input) {
    if (input.indexOf('-') > -1) {
      let inputRange = input.split('-');
      let start = inputRange[0];
      let end = inputRange[1];
      if (end < start) {
        start = inputRange[1];
        end = inputRange[0];
      }
      for (let i = Number(start); i <= end; i++) {
        this.save(i);
      }
    } else {
      this.save(Number(input));
    }
  }

  save(data) {
    let { duplicate, unique } = this.state;
    if (unique.indexOf(data) === -1) {
      unique.push(data);
    } else {
      duplicate.push(data);
    }
    this.setState({ duplicate, unique });
  }

  renderData(data, type) {
    if (!data || !data.length) {
      return null;
    }
    return (
      <div>
        <label className="tagHeading">{type}</label>
        <div className="tagContainer">
          {data.map((item, index) => (
            <label key={index} className={`tagItem ${type}`}>
              {item}
            </label>
          ))}
        </div>
      </div>
    );
  }

  render() {
    let { duplicate, unique } = this.state;
    return (
      <div className="App">
        <div className="mainContainer">
          <div className="inputWrapper">
            <div className="rowItem">{this.renderData(unique, 'unique')}</div>
            <div className="rowItem">
              <input onChange={this.setText} value={this.state.input} className="inputBox" placeholder="Enter value / Range" />
              <button onClick={this.addData} className="button">
                Insert
              </button>
            </div>
            <div className="rowItem">{this.renderData(duplicate, 'duplicate')}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
